<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\contacts;

class ContactsController extends Controller
{
    public function importGoogleContact()
    {
        $code = Request::get('code');

        $googleService = \OAuth::consumer('Google');

        if (!is_null($code)) {

            $token = $googleService->requestAccessToken($code);
            $result = json_decode($googleService->request('https://www.google.com/m8/feeds/contacts/default/full?alt=json&amp;max-results=400'), true);

            $emails = [];
            foreach ($result['feed']['entry'] as $contact) {
                if (isset($contact['gd$email'])) {
                    $emails[] = $contact['gd$email'][0]['address'];
                }
            }

            return $emails;
        }
    }
}