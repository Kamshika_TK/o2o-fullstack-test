<?php

namespace App\Http\Controllers;

use App\Models\Conversations;
use App\Models\Messages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;

class ConversationsController extends Controller
{
    public function index()
    {
        $user = User::all();
        $message = Messages::all();

        $conversaction = DB::table('conversations')
            ->select('conversations.id as c_id', 'conversations.title as title', 'users.*', 'messages.*')
            ->leftjoin('users', 'users.id', '=', 'conversations.owner_id')
            ->leftjoin('messages', 'messages.conversation_id', '=', 'conversations.id')
            ->orderBy('conversations.id', 'asc')
            ->get();

        // return $conversaction[0]->c_id;
        return view('home')
            ->with('user', $user)
            ->with('message', $message)
            ->with('conversaction', $conversaction);
    }
g
    public function createConvo()
    {
        $user = User::all();

        return view('home')
            ->with('user', $user);
    }

    public function createConversations(Request $request)
    {
        $user = Auth::user();

        $conversaction = new Conversations();
        $conversaction->owner_id = $user->id;
        $conversaction->title = $request->input('title');
        $conversaction->save();

        return redirect('/home');
    }

    public function viewConversations($id)
    {
        $person = Auth::User()->id;
        $user = User::all();
        $message = Messages::all();

        $conversaction = Conversations::find($id)
            ->select('conversations.*', 'conversations.title as title', 'users.*', 'users.name as uname', 'messages.*')
            ->leftjoin('users', 'users.id', '=', 'conversations.owner_id')
            ->leftjoin('messages', 'messages.conversation_id', '=', 'conversations.id')
            ->where('messages.conversation_id', $id)
            ->orderBy('conversations.id', 'asc')
            ->paginate();

        // return $conversaction;
        return view('home')
            ->with('user', $user)
            ->with('person', $person)
            ->with('message', $message)
            ->with('conversaction', $conversaction);
    }
}
