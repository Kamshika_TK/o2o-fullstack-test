<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Conversations;
use App\Models\Messages;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MessagesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function message($id)
    {
        $person = Auth::User()->id;
        $user = User::all();

        $message = Conversations::find($id)
            ->select('conversations.*', 'conversations.title as title', 'users.*', 'messages.body as body')
            ->leftjoin('users', 'users.id', '=', 'conversations.owner_id')
            ->leftjoin('messages', 'messages.conversation_id', '=', 'conversations.id')
            ->where('messages.conversation_id', $id)
            ->where('messages.person_id', '=', 'conversations.owner_id')
            ->orderBy('conversations.id', 'asc')
            ->get();

        // return $message;

        return view('home')
            ->with('user', $user)
            ->with('person', $person)
            ->with('message', $message);
    }

    public function sendmessage(Request $request, $id)
    {
        $user = Auth::user()->id;

        $dailyLimit = 5;
        $numberOfMessages = Messages::GroupBy('person_id', 'conversation_id')
            ->where('created_at', '>', Carbon::now()->subDays(1))->count();
        if ($numberOfMessages <= $dailyLimit) {
            $convo = Conversations::find($id)
                ->select('conversations.id as c_id')
                ->leftjoin('users', 'users.id', '=', 'conversations.owner_id')
                ->leftjoin('messages', 'messages.conversation_id', '=', 'conversations.id')
                ->where('messages.conversation_id', $id)
                ->where('messages.person_id', '=', 'conversations.owner_id')
                ->orderBy('conversations.id', 'asc')
                ->get();

            $message = new Messages();
            $message->person_id = $user;
            $message->conversation_id = $convo;
            $message->body = $request->input('body');
            $message->save();

            return redirect('/conversaction/{id}')
                ->with('success', 'msg sent');
        }
        else{
            return redirect('/conversaction/{id}')
            ->with('error', 'You have reached your maximum limit of message today');
        }
    }

    public function destroy($id)
    {
        $user = Auth::user()->id;
        $message = Messages::find($id)->where($user, $id);
        $message->delete();
        return $message;
    }
}
