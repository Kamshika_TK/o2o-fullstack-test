<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ChatsController;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\ConversationsController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\ParticipantsController;

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => config('fortify.middleware', ['web'])], function () {

    Route::get('/home', [ConversationsController::class, 'index']);
    Route::get('/createConversation', [ConversationsController::class, 'createConvo']);
    Route::post('sendconversaction', [ConversationsController::class, 'createConversations']);
    Route::get('/conversaction/{id}', [ConversationsController::class, 'viewConversations']);

    Route::get('/message/{id}', [MessagesController::class, 'message']);
    Route::get('/sendmessage', [MessagesController::class, 'sendmessage']);

    Route::get('/contact/import/google', [ContactsController::class, 'importGoogleContact']);

    Route::resources([
        'conversation' => ConversationsController::class,
        'user' => Controller::class,
        'message' => MessagesController::class,
    ]);
});